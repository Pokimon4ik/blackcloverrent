<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ObjectsRepository")
 */
class Objects
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\Column(type="integer")
     */
    private $Price;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Contract", mappedBy="Object", cascade={"persist", "remove"})
     */
    private $contract;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Mods", inversedBy="objects")
     */
    private $Mods;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Jurnal", mappedBy="Object")
     */
    private $jurnals;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="objects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Category;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Description;

    /**
     * @ORM\Column(type="text")
     */
    private $Url;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $Rating;

    public function __construct()
    {
        $this->Mods = new ArrayCollection();
        $this->jurnals = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->Price;
    }

    public function setPrice(int $Price): self
    {
        $this->Price = $Price;

        return $this;
    }

    public function getContract(): ?Contract
    {
        return $this->contract;
    }

    public function setContract(Contract $contract): self
    {
        $this->contract = $contract;

        // set the owning side of the relation if necessary
        if ($this !== $contract->getObject()) {
            $contract->setObject($this);
        }

        return $this;
    }

    /**
     * @return Collection|Mods[]
     */
    public function getMods(): Collection
    {
        return $this->Mods;
    }

    public function addMod(Mods $mod): self
    {
        if (!$this->Mods->contains($mod)) {
            $this->Mods[] = $mod;
        }

        return $this;
    }

    public function removeMod(Mods $mod): self
    {
        if ($this->Mods->contains($mod)) {
            $this->Mods->removeElement($mod);
        }

        return $this;
    }

    /**
     * @return Collection|Jurnal[]
     */
    public function getJurnals(): Collection
    {
        return $this->jurnals;
    }

    public function addJurnal(Jurnal $jurnal): self
    {
        if (!$this->jurnals->contains($jurnal)) {
            $this->jurnals[] = $jurnal;
            $jurnal->addObject($this);
        }

        return $this;
    }

    public function removeJurnal(Jurnal $jurnal): self
    {
        if ($this->jurnals->contains($jurnal)) {
            $this->jurnals->removeElement($jurnal);
            $jurnal->removeObject($this);
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->Category;
    }

    public function setCategory(?Category $Category): self
    {
        $this->Category = $Category;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->Url;
    }

    public function setUrl(string $Url): self
    {
        $this->Url = $Url;

        return $this;
    }

    public function __toString()
    {
        return $this->Name;
    }

    public function getRating(): ?float
    {
        return $this->Rating;
    }

    public function setRating(?float $Rating): self
    {
        $this->Rating = $Rating;

        return $this;
    }

}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContractRepository")
 */
class Contract
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $NowDate;

    /**
     * @ORM\Column(type="date")
     */
    private $StartDateRent;

    /**
     * @ORM\Column(type="date")
     */
    private $EndDateRent;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Objects", inversedBy="contract", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $Object;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RentLoc", inversedBy="contracts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $RentLoc;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Action;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RentLoc")
     */
    private $EstimLoc;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="contracts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Client;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNowDate(): ?\DateTimeInterface
    {
        return $this->NowDate;
    }

    public function setNowDate(\DateTimeInterface $NowDate): self
    {
        $this->NowDate = $NowDate;

        return $this;
    }

    public function getStartDateRent(): ?\DateTimeInterface
    {
        return $this->StartDateRent;
    }

    public function setStartDateRent(\DateTimeInterface $StartDateRent): self
    {
        $this->StartDateRent = $StartDateRent;

        return $this;
    }

    public function getEndDateRent(): ?\DateTimeInterface
    {
        return $this->EndDateRent;
    }

    public function setEndDateRent(\DateTimeInterface $EndDateRent): self
    {
        $this->EndDateRent = $EndDateRent;

        return $this;
    }

    public function getObject(): ?Objects
    {
        return $this->Object;
    }

    public function setObject(Objects $Object): self
    {
        $this->Object = $Object;

        return $this;
    }

    public function getRentLoc(): ?RentLoc
    {
        return $this->RentLoc;
    }

    public function setRentLoc(?RentLoc $RentLoc): self
    {
        $this->RentLoc = $RentLoc;

        return $this;
    }

    public function getAction(): ?bool
    {
        return $this->Action;
    }

    public function setAction(bool $Action): self
    {
        $this->Action = $Action;

        return $this;
    }

    public function getEstimLoc(): ?RentLoc
    {
        return $this->EstimLoc;
    }

    public function setEstimLoc(?RentLoc $EstimLoc): self
    {
        $this->EstimLoc = $EstimLoc;

        return $this;
    }

    public function getClient(): ?User
    {
        return $this->Client;
    }

    public function setClient(?User $Client): self
    {
        $this->Client = $Client;

        return $this;
    }

}

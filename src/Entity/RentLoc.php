<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RentLocRepository")
 */
class RentLoc
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Phone;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contract", mappedBy="RentLoc")
     */
    private $contracts;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Jurnal", mappedBy="EndLoc")
     */
    private $EndJurnals;



    public function __construct()
    {
        $this->contracts = new ArrayCollection();
        $this->EndJurnals = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->Address;
    }

    public function setAddress(string $Address): self
    {
        $this->Address = $Address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->Phone;
    }

    public function setPhone(?string $Phone): self
    {
        $this->Phone = $Phone;

        return $this;
    }

    /**
     * @return Collection|Contract[]
     */
    public function getContracts(): Collection
    {
        return $this->contracts;
    }

    public function addContract(Contract $contract): self
    {
        if (!$this->contracts->contains($contract)) {
            $this->contracts[] = $contract;
            $contract->setRentLoc($this);
        }

        return $this;
    }

    public function removeContract(Contract $contract): self
    {
        if ($this->contracts->contains($contract)) {
            $this->contracts->removeElement($contract);
            // set the owning side to null (unless already changed)
            if ($contract->getRentLoc() === $this) {
                $contract->setRentLoc(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Jurnal[]
     */
    public function getEndJurnals(): Collection
    {
        return $this->EndJurnals;
    }

    public function addEndJurnal(Jurnal $endJurnal): self
    {
        if (!$this->EndJurnals->contains($endJurnal)) {
            $this->EndJurnals[] = $endJurnal;
            $endJurnal->addEndLoc($this);
        }

        return $this;
    }

    public function removeEndJurnal(Jurnal $endJurnal): self
    {
        if ($this->EndJurnals->contains($endJurnal)) {
            $this->EndJurnals->removeElement($endJurnal);
            $endJurnal->removeEndLoc($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getAddress();
    }
}

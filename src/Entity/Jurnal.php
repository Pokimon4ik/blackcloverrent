<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\JurnalRepository")
 */
class Jurnal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $action;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Date;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\objects", inversedBy="jurnals")
     */
    private $Object;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\RentLoc", inversedBy="EndJurnals")
     */
    private $EndLoc;

    public function __construct()
    {
        $this->Object = new ArrayCollection();
        $this->EndLoc = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAction(): ?bool
    {
        return $this->action;
    }

    public function setAction(bool $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    /**
     * @return Collection|objects[]
     */
    public function getObject(): Collection
    {
        return $this->Object;
    }

    public function addObject(objects $object): self
    {
        if (!$this->Object->contains($object)) {
            $this->Object[] = $object;
        }

        return $this;
    }

    public function removeObject(objects $object): self
    {
        if ($this->Object->contains($object)) {
            $this->Object->removeElement($object);
        }

        return $this;
    }

    /**
     * @return Collection|RentLoc[]
     */
    public function getEndLoc(): Collection
    {
        return $this->EndLoc;
    }

    public function addEndLoc(RentLoc $endLoc): self
    {
        if (!$this->EndLoc->contains($endLoc)) {
            $this->EndLoc[] = $endLoc;
        }

        return $this;
    }

    public function removeEndLoc(RentLoc $endLoc): self
    {
        if ($this->EndLoc->contains($endLoc)) {
            $this->EndLoc->removeElement($endLoc);
        }

        return $this;
    }


}

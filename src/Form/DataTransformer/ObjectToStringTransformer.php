<?php
namespace App\Form\DataTransformer;

use App\Entity\Objects;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ObjectToStringTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms an object ($Objects) to a string (number).
     *
     * @param  Objects|null $Objects
     * @return string
     */
    public function transform($Objects)
    {
        if (null === $Objects) {
            return '';
        }

        return $Objects->getId();
    }

    /**
     * Transforms a string (number) to an object ($Objects).
     *
     * @param  string $ObjId
     * @return Objects|null
     * @throws TransformationFailedException if object ($Objects) is not found.
     */
    public function reverseTransform($ObjId)
    {
        // no Objects number? It's optional, so that's ok
        if (!$ObjId) {
            return;
        }

        $Objects = $this->entityManager
            ->getRepository(Objects::class)
            // query for the Objects with this id
            ->find($ObjId)
        ;

        if (null === $Objects) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An Objects with number "%s" does not exist!',
                $ObjId
            ));
        }

        return $Objects;
    }
}

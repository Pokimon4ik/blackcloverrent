<?php

namespace App\Form;

use App\Entity\Contract;
use App\Form\DataTransformer\ObjectToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContractType extends AbstractType
{
    private $transformer;

    public function __construct(ObjectToStringTransformer $transformer)
    {
        $this->transformer = $transformer;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Object', HiddenType::class)
            ->add('StartDateRent')
            ->add('EndDateRent')
            ->add('RentLoc')
            ->add('EstimLoc')
        ;
        $builder->get('Object')->addModelTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contract::class,
        ]);
    }
}

<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NovbarController extends AbstractController
{
    /**
     * @Route("/novbar", name="novbar")
     */
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('novbar/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
        ]);
    }
}

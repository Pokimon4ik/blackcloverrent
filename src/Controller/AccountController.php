<?php

namespace App\Controller;

use App\Repository\ContractRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="account")
     */
    public function index(ContractRepository $contractRepository): Response
    {
        $contracts = $contractRepository->findAll();


        return $this->render('account/index.html.twig', [
            'controller_name' => 'AccountController',
            'contracts' => $this->getUser()->getContracts()
        ]);
    }
}

<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Objects;
use App\Repository\CategoryRepository;
use PhpCollection\Set;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpCache\Store;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function index(CategoryRepository $categoryRepository): Response
    {

        $categorys = $categoryRepository->findAll();
        $temp = new Set();
        foreach ($categorys as &$category){
            $objectMaxRating = new Objects();
            $objectMaxRating->setRating(0);
            $objects = $category->getObjects();
            foreach ($objects as &$object){
                if ($objectMaxRating->getRating() <= $object->getRating()){
                    $objectMaxRating = $object;
                }
            }
            if ($objectMaxRating->getId() != null)
            $temp->add($objectMaxRating);
    }
        $temp2 = $temp->all();
        dump($temp2[0]);

        return $this->render('home_page/index.html.twig', [
            'controller_name' => 'HomePageController',
            'categories' => $categoryRepository->findAll(),
            'objectMaxRating' => $temp->all(),
        ]);
    }
}

<?php

namespace App\Repository;

use App\Entity\StartRenLocJurnal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method StartRenLocJurnal|null find($id, $lockMode = null, $lockVersion = null)
 * @method StartRenLocJurnal|null findOneBy(array $criteria, array $orderBy = null)
 * @method StartRenLocJurnal[]    findAll()
 * @method StartRenLocJurnal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StartRenLocJurnalRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StartRenLocJurnal::class);
    }

    // /**
    //  * @return StartRenLocJurnal[] Returns an array of StartRenLocJurnal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StartRenLocJurnal
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

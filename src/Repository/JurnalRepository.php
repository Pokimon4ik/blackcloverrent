<?php

namespace App\Repository;

use App\Entity\Jurnal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Jurnal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Jurnal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Jurnal[]    findAll()
 * @method Jurnal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JurnalRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Jurnal::class);
    }

    // /**
    //  * @return Jurnal[] Returns an array of Jurnal objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Jurnal
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
